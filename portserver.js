const express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var proj = require('./public/projet.json');
const app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json('application/json'));

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/index.html', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/projets.html', function(req, res) {
	res.sendFile(path.join(__dirname + '/projets.html'));
});

app.get('/partials/navBar', function(req, res) {
	res.sendFile(path.join(__dirname + '/partials/navBar.html'));
});

app.post('/getProjects', function(req, res) {
	res.send(proj);
});
app.post('/projetCv', function(req, res) {
	res.send(proj);
});

app.get('/contact.html', function(req, res) {
	res.sendFile(path.join(__dirname + '/contact.html'));
});
app.get('/synth.html', function(req, res) {
	res.sendFile(path.join(__dirname + '/synth.html'));
});
app.get('/cash.html', function(req, res) {
	res.sendFile(path.join(__dirname + '/cash.html'));
});

app.listen(8500, () => console.log('Now listening to 8500'));
