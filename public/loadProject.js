$( document ).ready(function() {
    $.ajax({
        type: 'POST',
        url: '/getProjects', 
        success: function(result) {
			parseProj(result);
			animateHTML().init();
        },
    });
})

function parseProj(obj){
	var result=""
	var count=0;
	for (var key in obj){
		result+=count>2?"<dl class='hidden'>":"<dl>";
		result+="<dt>"+key+"</dt><hr>";
		result+="<dd>";
		if(obj[key]["link"]){
			result+=obj[key]["link"]+"<br>"
		}
		if(obj[key]["repo"]){
			result+=obj[key]["repo"]+"<br>"
		}
		result+=obj[key]["description"];
		result+="</dd>"
		result+="</dl>"
		count++
	}
	$("div.loadProjects").html(result+"<br>");
}


