window.onload=function(){
	//functions at loading
	$("#shape").text ("square");
	$("#dualShape1").text ("square");
	$("#dualShape2").text ("square");

	var oscPitch = $("#oscPitch").val();
	$('#pitch').text(oscPitch +" hertz");
	filterControl();
	volume();
	
}


var context = new AudioContext();
var oscillator;
var balise = 0;
var shape;
var biquadFilter = context.createBiquadFilter();

//mixer section
masterGain = context.createGain();
masterGain.connect(context.destination);

osc1Gain = context.createGain();
osc1Gain.connect(biquadFilter);

osc2Gain = context.createGain();
osc2Gain.connect(biquadFilter);

/*
TODO
Implement LFO Controls
*/


//volume control
volume = function(){
	masterGain.gain.value = $("#masterVolume").val();
	osc1Gain.gain.value = $("#Osc1Volume").val();
	osc2Gain.gain.value = $("#Osc2Volume").val();

};

//filter selection
filterType = function(){
	type = $("#filterType").val();

		if (type==1){type = "lowpass";}
		if (type==2){type = "highpass";}
		if (type==3){type = "bandpass";}
		if (type==4){type = "lowshelf";}
		if (type==5){type = "highshelf";}
		if (type==6){type = "peaking";}
		if (type==7){type = "notch";}
		if (type==8){type = "allpass";}

	$("#filter").text(type);
	return type;
};


filterControl = function(){
	biquadFilter.type= filterType();
	biquadFilter.frequency.value = $("#filterFreq").val();
	biquadFilter.q = $("#filterQ").val();
	biquadFilter.gain.value = $("#filterGain").val();
	biquadFilter.connect(masterGain);
			
};



//tone selection
tone = function(input,ouput){
	shape = input
		if(shape==1){shape = "sine";}
		if(shape==2){shape = "square";}
		if(shape==3){shape = "sawtooth";}
		if(shape==4){shape = "triangle";}

	$(ouput).text(shape);
	
	return shape;

};


oscDisc = function(){
	//disconnection function
	oscillator.disconnect();

};


dualSynth = function(freq){
	//dual oscillator controls
	filterControl();
	oscillator1 = context.createOscillator();
	oscillator1.type = tone($("#oscShape1").val(),"#dualShape1");
	var frequency1 = $("#octaver1").val()*freq + +$("#detuner1").val();
	oscillator1.frequency.value =$("#octaver1").val()*freq + +$("#detuner1").val()
	oscillator1.connect(osc1Gain);
	oscillator1.start(0)

	oscillator2 = context.createOscillator();
	oscillator2.type = tone($("#oscShape2").val(),"#dualShape2");
	oscillator2.frequency.value =$("#octaver2").val()*freq+ +$("#detuner2").val()

	oscillator2.connect(osc2Gain)
	oscillator2.start(0)

	document.addEventListener('keyup', function(evenement) {
	 	this.className = '';
		oscillator1.disconnect();
		oscillator2.disconnect();
		   
});

};


document.addEventListener('keydown', function(evenement) {
 
    if ( this.className === 'hold' ) { return false; }
    this.className = 'hold'

	switch(evenement.key)
	{
		case "a": 	//C
			dualSynth(65.41)     
			break;	
		case "s": //D		
			dualSynth(73.42)
			break;
		case "d":	//E	
			dualSynth(82.41)
			break;				
		case "f": 	//F		
			dualSynth(87.31)
			break;	
		case "g": 	//G	
			dualSynth(98.00)
			break;	
		case "h": 	//A	
			dualSynth(110.00)
			break;	
		case "j": 	//B	
			dualSynth(123.47)
			break;	
		case "k": 	//C	
			dualSynth(130.81)
			break;	

		case "q": 	//C
			dualSynth(130.81)     
			break;	
		case "w": //D		
			dualSynth(146.83)
			break;
		case "e":	//E	
			dualSynth(164.81)
			break;				
		case "r": 	//F		
			dualSynth(174.61)
			break;	
		case "t": 	//G	
			dualSynth(196.00)
			break;	
		case "y": 	//A	
			dualSynth(220.00)
			break;	
		case "u": 	//B	
			dualSynth(246.94)
			break;	
		case "i": 	//C	
			dualSynth(261.63)
			break;	

	}
	   
});




